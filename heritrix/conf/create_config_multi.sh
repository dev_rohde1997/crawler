#!/bin/sh
# Creating individual crawler config file
echo Creating crawler-beans.cxml...

cat ../conf/config_part_1.txt > crawler-beans.cxml

echo -n Enter contact adress:
read contact

echo $contact >> crawler-beans.cxml

cat ../conf/config_part_2.txt >> crawler-beans.cxml

echo -n Enter URL: 
read url


while [ "$url" != "exit" ]
do
    echo $url >> crawler-beans.cxml
    echo "Enter next URL: (If you entered all URLs tpye exit)"
    read url
done

cat ../conf/config_part_3.txt >> crawler-beans.cxml

echo Config file generated.



