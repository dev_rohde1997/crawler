#!/bin/sh
# After a job is done there has to be some clean ups

job=fme
arc_dir=/root/docker_test/openwayback/arcs
tmp=/root/docker_test/openwayback/tmp
warc_dir=$tmp/$job/2*/warcs/*



echo Starting After job clean up
# Moving warc files to $tmp
docker cp docker_test_heritrix_1:/jobs/$job/ $tmp



new_folders="$(find $tmp/$job -name '2*')"

if [ -z "$new_folders" ];then 
    echo No new warc files deteced 
else
    echo New warc files deteced

     # Moving arc files intern
    echo Moving only the warc files to local openwayback warc directory

    for subfolder in $new_folders 
    do
        warc_files="$(find $subfolder/warcs -name '*.gz')"

        for files in $warc_files 
        do
            mv $files $arc_dir
        done
        #TODO: Copy log files
        # mv $subfolder/log $arcdir/$subfolder/logs ??
    done
    rm -rf $tmp/*

    # Removing heritrix job results
    # docker exec -i docker_test_heritrix_1 sh -c 'rm -rf /jobs/$job/2*'
    echo The warc directories has been removed
    
    # Restarting openwayback container
    cd ~/docker_test
    docker-compose restart openwayback

fi


