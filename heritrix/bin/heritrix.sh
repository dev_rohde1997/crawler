#!/bin/sh
# Shell file for a basic curl communication to hertrix3.4-0
# Heritrix ist running local at port 8443


echo Executing hertrix shell script
# list of valid command options
commands='build launch pause unpause checkpoint terminate teardown new conf'

# Defining vars
action=$1
job=$2
#absolute path of crawler config file needed
crawler_path=/heritrix-3.4.0-20190418/bin/crawler-beans.cxml 
#Login vars
: ${HERITRIX_USER:=heritrix}
: ${HERITRIX_PASSWORD:=heritrix}

# Option validation 
case "$action" in
    build|launch|pause|unpause|checkpoint|terminate|teardown) 
         echo "Your option is: $action"
         curl -v -d "action=$action" -k -u $HERITRIX_USER:$HERITRIX_PASSWORD --anyauth --location https://localhost:8443/engine/job/$job > heritrix_curl_log.txt        ;;
    new) echo "You are trying to create a new job - BETA"  
         echo "The job will be called $job"
         curl -v -d "createpath=$job&action=create" -k -u $HERITRIX_USER:$HERITRIX_PASSWORD --anyauth --location https://localhost:8443/engine                          ;;
    conf)
         echo "Changing jobs configuration - BETA"
         curl -v -T $crawler_path -k -u $HERITRIX_USER:$HERITRIX_PASSWORD --anyauth --location https://localhost:8443/engine/job/$job/jobdir/crawler-beans.cxml >  heritrix_conf_log.txt    ;;   
    *)   echo Your action is not valid
	     echo valid options are: $commands                                                                                                                              ;;

esac


echo shell script finished
