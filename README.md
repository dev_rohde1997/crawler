# Heritrix and Openwayback 

This project contains an two applications packed into docker.

- [x] Heritrix available at **port 8443**
- [x] Openwayback available at **port 8080**

## Heritrix - Web Archiving

Heritrix is the Internet Archive's open-source, extensible, web-scale, archival-quality web crawler project. Heritrix (sometimes spelled heretrix, or misspelled or missaid as heratrix/heritix/heretix/heratix) is an archaic word for heiress (woman who inherits). Since our crawler seeks to collect and preserve the digital artifacts of our culture for the benefit of future researchers and generations, this name seemed apt.

See: [Heritrix Github](https://github.com/internetarchive/heritrix3)

Heritrix was automated using an individual [script](/heritrix/bin/heritrix.sh) and cron.

## Openwayback - Web crawler

OpenWayback is the open source project aimed to develop Wayback Machine, the key software used by web archives worldwide to play back archived websites in the user's browser. OpenWayback is supported by the members of the International Internet Preservation Consortium (IIPC).

See: [Openwayback Github](https://github.com/iipc/openwayback)
